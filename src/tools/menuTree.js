export default [
  {
    'id': 'platform-business-manager',
    'label': '平台业务办理',
    'children': [
      {
        'id': 'factory-order-info',
        'label': '出入库明细',
        'perms': 'factory-order-info',
        'children': [
          {
            'id': 'factory-order-info-export',
            'label': '导出出入库明细',
            'perms': 'factory-order-info-export'
          },
          {
            'id': 'mechanical-in-export',
            'label': '导出器械入库信息',
            'perms': 'mechanical-in-export'
          },
          {
            'id': 'mechanical-out-export',
            'label': '导出器械出库信息',
            'perms': 'mechanical-out-export'
          }
        ]
      },
      {
        'id': 'out-order-manager',
        'label': '出库单管理',
        'children': [
          {
            'id': 'oms-out-order-add',
            'label': '新增出库订单',
            'perms': 'oms-out-order-add,oms-manufacture-out-order-add'
          },
          {
            'id': 'oms-out-order-query',
            'label': '查看出库单',
            'perms': 'oms-out-order-query'
          },
          {
            'id': 'oms-out-order-edit',
            'label': '编辑出库订单',
            'perms': 'oms-out-order-edit'
          },
          {
            'id': 'oms-out-order-trace-code-manager',
            'label': '追溯码管理',
            'children': [
              {
                'id': 'oms-out-order-trace-code-upload',
                'label': '追溯码文件上传',
                'perms': 'oms-out-order-trace-code-upload'
              },
              {
                'id': 'oms-out-order-trace-code-download',
                'label': '下载追溯码文件',
                'perms': 'oms-out-order-trace-code-download'
              }
            ],
            'perms': 'oms-out-order-trace-code-manager'
          },
          {
            'id': 'oms-out-order-document-manager',
            'label': '附件管理',
            'children': [
              {
                'id': 'oms-out-order-document-upload',
                'label': '上传订单附件',
                'perms': 'oms-out-order-document-upload'
              },
              {
                'id': 'oms-out-order-document-download',
                'label': '下载订单附件',
                'perms': 'oms-out-order-document-download'
              },
              {
                'id': 'oms-out-order-document-watch',
                'label': '查看订单附件',
                'perms': 'oms-out-order-document-watch'
              }
            ],
            'perms': 'oms-out-order-document-manager'
          },
          {
            'id': 'oms-out-order-status-cancel',
            'label': '取消出库订单',
            'perms': 'oms-out-order-status-cancel,oms-manufacture-out-order-status-cancel'
          },
          {
            'id': 'oms-out-order-status-confirm',
            'label': '出库订单确认',
            'perms': 'oms-out-order-status-confirm'
          },
          {
            'id': 'oms-out-order-batch-number-upload',
            'label': '批号关联文件管理',
            'perms': 'oms-out-order-batch-number-upload'
          },
          {
            'id': 'oms-out-order-status-sign',
            'label': '出库订单签收',
            'perms': 'oms-out-order-status-sign'
          }
        ],
        'perms': 'out-order-manager'
      },
      {
        'id': 'org-goods-manager',
        'label': '货主货品管理',
        'children': [
          {
            'id': 'org-goods-manager-watch',
            'label': '查看货主货品',
            'perms': 'org-goods-manager-watch'
          },
          {
            'id': 'org-goods-manager-edit',
            'label': '编辑货主货品',
            'perms': 'org-goods-manager-edit'
          },
          {
            'id': 'org-goods-manager-add',
            'label': '新增货主货品',
            'perms': 'org-goods-manager-add'
          }
        ],
        'perms': 'org-goods-manager'
      },
      {
        'id': 'refuse-documents-info',
        'label': '拒收单信息',
        'perms': 'refuse-documents-info',
        'children': [
          {
            'id': 'oms-in-order-rejection_download',
            'label': '查看货主',
            'perms': 'oms-in-order-rejection_download'
          },
          {
            'id': 'refuse-documents-export',
            'label': '查看货主',
            'perms': 'refuse-documents-export'
          }
        ]
      },
      {
        'id': 'quality-check-log',
        'label': '质量验收记录',
        'perms': 'quality-check-log'
      },
      {
        'id': 'goods-owner-manager',
        'label': '货主管理',
        'children': [
          {
            'id': 'oms-goods-owner-query',
            'label': '查看货主',
            'perms': 'oms-goods-owner-query'
          }
        ],
        'perms': 'goods-owner-manager'
      },
      {
        'id': 'in-order-manager',
        'label': '入库单管理',
        'children': [
          {
            'id': 'oms-in-order-status-stock',
            'label': '入库订单货品上架',
            'perms': 'oms-in-order-status-stock'
          },
          {
            'id': 'oms-in-order-status-acceptance',
            'label': '入库订单质量验收',
            'perms': 'oms-in-order-status-acceptance'
          },
          {
            'id': 'oms-in-order-status-check-on-site',
            'label': '入库订单现场验收',
            'perms': 'oms-in-order-status-check-on-site'
          },
          {
            'id': 'oms-in-order-status-rejection',
            'label': '入库订单拒收',
            'perms': 'oms-in-order-status-rejection'
          },
          {
            'id': 'oms-in-order-sample-add',
            'label': '新增入库订单抽样信息',
            'perms': 'oms-in-order-sample-add'
          },
          {
            'id': 'oms-in-order-pdf-download',
            'label': '下载入库订单验收记录',
            'perms': 'oms-in-order-pdf-download'
          },
          {
            'id': 'oms-in-order-quality-manager',
            'label': '质量检查项',
            'children': [
              {
                'id': 'oms-in-order-quality-save',
                'label': '编辑入库订单质检',
                'perms': 'oms-in-order-quality-save'
              },
              {
                'id': 'oms-in-order-quality-watch',
                'label': '查看入库订单质检',
                'perms': 'oms-in-order-quality-watch'
              }
            ],
            'perms': 'oms-in-order-quality-manager'
          },
          {
            'id': 'oms-in-order-conversions',
            'label': '采购订单转换销售订单',
            'perms': 'oms-in-order-conversions'
          },
          {
            'id': 'oms-in-order-status-stop-reject',
            'label': '入库订单中止拒收',
            'perms': 'oms-in-order-status-stop-reject'
          },
          {
            'id': 'oms-in-order-status-check-by-file',
            'label': '入库订单文件验收',
            'perms': 'oms-in-order-status-check-by-file'
          },
          {
            'id': 'oms-in-order-status-rejection-review',
            'label': '入库订单拒收复核',
            'perms': 'oms-in-order-status-rejection-review'
          },
          {
            'id': 'oms-in-order-trace-code-manager',
            'label': '追溯码管理',
            'children': [
              {
                'id': 'oms-in-order-trace-code-upload',
                'label': '追溯码文件上传',
                'perms': 'oms-in-order-trace-code-upload'
              },
              {
                'id': 'oms-in-order-trace-code-download',
                'label': '下载追溯码文件',
                'perms': 'oms-in-order-trace-code-download'
              }
            ],
            'perms': 'oms-in-order-trace-code-manager'
          },
          {
            'id': 'oms-in-order-add',
            'label': '新增入库订单',
            'perms': 'oms-in-order-add,oms-manufacture-in-order-add'
          },
          {
            'id': 'oms-in-order-receipt-manager',
            'label': '收货详情',
            'children': [
              {
                'id': 'receipt-add',
                'label': '新增收货信息',
                'perms': 'receipt-add'
              },
              {
                'id': 'receipt-edit',
                'label': '编辑收货信息',
                'perms': 'receipt-edit'
              },
              {
                'id': 'receipt-delete',
                'label': '删除收货信息',
                'perms': 'receipt-delete'
              }
            ],
            'perms': 'oms-in-order-receipt-manager'
          },
          {
            'id': 'oms-in-order-sample-edit',
            'label': '编辑入库订单抽样信息',
            'perms': 'oms-in-order-sample-edit'
          },
          {
            'id': 'oms-in-order-status-receipt',
            'label': '入库订单收货',
            'perms': 'oms-in-order-status-receipt'
          },
          {
            'id': 'oms-in-order-status-org-confirm-reject',
            'label': '货主确认拒收',
            'perms': 'oms-in-order-status-org-confirm-reject'
          },
          {
            'id': 'oms-in-order-batch-number-manager',
            'label': '批号管理',
            'children': [
              {
                'id': 'oms-batch-number-edit',
                'label': '编辑批号',
                'perms': 'oms-batch-number-edit'
              },
              {
                'id': 'oms-batch-number-add',
                'label': '新增批号',
                'perms': 'oms-batch-number-add'
              },
              {
                'id': 'oms-in-order-batch-batch-number-download',
                'label': '批号关联文件下载',
                'perms': 'oms-in-order-batch-batch-number-download'
              },
              {
                'id': 'oms-batch-number-delete',
                'label': '删除批号',
                'perms': 'oms-batch-number-delete'
              },
              {
                'id': 'oms-in-order-batch-number-upload',
                'label': '批号关联文件管理',
                'perms': 'oms-in-order-batch-number-upload'
              }
            ],
            'perms': 'oms-in-order-batch-number-manager'
          },
          {
            'id': 'oms-in-order-status-examine',
            'label': '审核入库订单',
            'perms': 'oms-in-order-status-examine'
          },
          {
            'id': 'oms-in-order-query',
            'label': '查看入库单',
            'perms': 'oms-in-order-query'
          },
          {
            'id': 'oms-in-order-status-confirm',
            'label': '确认入库订单',
            'perms': 'oms-in-order-status-confirm,oms-manufacture-in-order-status-confirm'
          },
          {
            'id': 'oms-in-order-status-org-confirm-receipt',
            'label': '入库订单拒收取消',
            'perms': 'oms-in-order-status-org-confirm-receipt'
          },
          {
            'id': 'oms-in-order-status-cancel',
            'label': '取消入库订单',
            'perms': 'oms-in-order-status-cancel,oms-manufacture-in-order-status-cancel'
          },
          {
            'id': 'oms-in-order-reject_download',
            'label': '下载销售退货单',
            'perms': 'oms-in-order-reject_download'
          },
          {
            'id': 'oms-in-order-one_vaccine_download',
            'label': '下载一类苗出库运输单',
            'perms': 'oms-in-order-one_vaccine_download'
          },
          {
            'id': 'oms-in-order-sample-delete',
            'label': '删除入库订单抽样信息',
            'perms': 'oms-in-order-sample-delete'
          },
          {
            'id': 'oms-in-order-document-manager',
            'label': '附件管理',
            'children': [
              {
                'id': 'oms-in-order-document-watch',
                'label': '查看入库订单附件',
                'perms': 'oms-in-order-document-watch'
              },
              {
                'id': 'oms-in-order-document-download',
                'label': '下载入库订单附件',
                'perms': 'oms-in-order-document-download'
              },
              {
                'id': 'oms-in-order-document-upload',
                'label': '上传入库订单附件',
                'perms': 'oms-in-order-document-upload'
              }
            ],
            'perms': 'oms-in-order-document-manager'
          },
          {
            'id': 'oms-in-order-edit',
            'label': '编辑入库订单',
            'perms': 'oms-in-order-edit'
          }
        ],
        'perms': 'in-order-manager'
      },
      {
        'id': 'quality-exception-all-manager',
        'label': '异常管理',
        'children': [
          {
            'id': 'quality-exception-all-close',
            'label': '关闭订单异常',
            'perms': 'quality-exception-all-close'
          },
          {
            'id': 'quality-exception-all-add',
            'label': '新增订单异常',
            'perms': 'quality-exception-all-add'
          },
          {
            'id': 'quality-exception-all-edit',
            'label': '提交异常反馈',
            'perms': 'quality-exception-all-edit'
          },
          {
            'id': 'quality-exception-all-cancel',
            'label': '取消订单异常',
            'perms': 'quality-exception-all-cancel'
          },
          {
            'id': 'quality-exception-all-push',
            'label': '推送客户订单异常',
            'perms': 'quality-exception-all-push'
          },
          {
            'id': 'quality-exception-all-confirm',
            'label': '确认订单异常',
            'perms': 'quality-exception-all-confirm'
          }
        ],
        'perms': 'quality-exception-all-manager'
      }
    ],
    'perms': 'platform-business-manager'
  },
  {
    'id': 'oms-business-info',
    'label': '基础资料库',
    'children': [
      {
        'id': 'oms-goods-manager',
        'label': '货品管理',
        'children': [
          {
            'id': 'oms-goods-add',
            'label': '新增货品',
            'perms': 'oms-goods-add'
          },
          {
            'id': 'oms-goods-edit',
            'label': '编辑货品',
            'perms': 'oms-goods-edit'
          },
          {
            'id': 'oms-goods-check',
            'label': '审核货品',
            'perms': 'oms-goods-check'
          },
          {
            'id': 'oms-goods-delete',
            'label': '删除货品',
            'perms': 'oms-goods-delete'
          }
        ],
        'perms': 'oms-goods-manager'
      },
      {
        'id': 'oms-business-unit',
        'label': '业务单位',
        'children': [
          {
            'id': 'goods-owner-out-order',
            'label': '出库订单',
            'children': [
              {
                'id': 'goods-owner-out-order-query',
                'label': '查看出库订单',
                'perms': 'goods-owner-out-order-query'
              }
            ],
            'perms': 'goods-owner-out-order'
          },
          {
            'id': 'goods-owner-stock-detail',
            'label': '出入库明细',
            'perms': 'goods-owner-stock-detail'
          },
          {
            'id': 'goods-owner-relation',
            'label': '业务关系',
            'children': [
              {
                'id': 'org-relation-delete',
                'label': '删除业务关系',
                'perms': 'org-relation-delete,oms-manufacture-org-relation-delete'
              },
              {
                'id': 'org-relation-edit',
                'label': '编辑业务关系',
                'perms': 'org-relation-edit,oms-manufacture-org-relation-edit'
              },
              {
                'id': 'org-relation-add',
                'label': '新增业务关系',
                'perms': 'org-relation-add,oms-manufacture-org-relation-add'
              },
              {
                'id': 'org-relation-check',
                'label': '审核业务关系',
                'perms': 'org-relation-check,oms-manufacture-org-relation-check'
              }
            ],
            'perms': 'goods-owner-relation'
          },
          {
            'id': 'goods-owner-quality-programmer',
            'label': '质检方案',
            'perms': 'goods-owner-quality-programmer'
          },
          {
            'id': 'oms-business-unit-delete',
            'label': '删除单位',
            'perms': 'oms-business-unit-delete'
          },
          {
            'id': 'goods-owner-in-order',
            'label': '入库订单',
            'children': [
              {
                'id': 'goods-owner-in-order-query',
                'label': '查看入库订单',
                'perms': 'goods-owner-in-order-query'
              }
            ],
            'perms': 'goods-owner-in-order'
          },
          {
            'id': 'goods-owner-bankInfo',
            'label': '财务信息',
            'children': [
              {
                'id': 'org-finance-watch',
                'label': '查看单位财务信息',
                'perms': 'org-finance-watch,oms-manufacture-org-finance-watch'
              },
              {
                'id': 'org-bankInfo-forbid',
                'label': '停用银行信息',
                'perms': 'org-bankInfo-forbid,oms-manufacture-org-bankInfo-forbid'
              },
              {
                'id': 'org-bankInfo-start',
                'label': '启用银行信息',
                'perms': 'org-bankInfo-start,oms-manufacture-org-bankInfo-start'
              },
              {
                'id': 'org-finance-audit',
                'label': '审核单位财务信息',
                'perms': 'org-finance-audit,oms-manufacture-org-finance-audit'
              },
              {
                'id': 'org-bankInfo-update',
                'label': '编辑银行信息',
                'perms': 'org-bankInfo-update,oms-manufacture-org-bankInfo-update'
              },
              {
                'id': 'org-finance-add',
                'label': '新增单位财务信息',
                'perms': 'org-finance-add,oms-manufacture-org-finance-add'
              },
              {
                'id': 'org-bankInfo-quick-audit',
                'label': '一键审核银行信息',
                'perms': 'org-bankInfo-quick-audit,oms-manufacture-org-bankInfo-audit'
              },
              {
                'id': 'org-finance-update',
                'label': '编辑单位财务信息',
                'perms': 'org-finance-update,oms-manufacture-org-finance-update'
              },
              {
                'id': 'org-bankInfo-audit',
                'label': '审核银行信息',
                'perms': 'org-bankInfo-audit,oms-manufacture-org-bankInfo-audit'
              },
              {
                'id': 'org-bankInfo-add',
                'label': '新增银行信息',
                'perms': 'org-bankInfo-add,oms-manufacture-org-bankInfo-add'
              },
              {
                'id': 'org-bankInfo-query',
                'label': '查询银行信息',
                'perms': 'org-bankInfo-query,oms-manufacture-org-bankInfo-query'
              }
            ],
            'perms': 'goods-owner-bankInfo'
          },
          {
            'id': 'goods-owner-goodsInfo',
            'label': '货品资料',
            'children': [
              {
                'id': 'org-goods-delete,',
                'label': '删除货主货品信息',
                'perms': 'org-goods-delete,oms-manufacture-org-goods-delete'
              },
              {
                'id': 'org-goods-edit',
                'label': '编辑货主货品信息',
                'perms': 'org-goods-edit,oms-manufacture-org-goods-edit'
              },
              {
                'id': 'org-goods-check',
                'label': '审核货主货品信息',
                'perms': 'org-goods-check,oms-manufacture-org-goods-check'
              },
              {
                'id': 'org-goods-add',
                'label': '新增货主货品信息',
                'perms': 'org-goods-add,oms-manufacture-org-goods-add'
              }
            ],
            'perms': 'goods-owner-goodsInfo'
          },
          {
            'id': 'goods-owner-baseInfo',
            'label': '基础信息',
            'children': [
              {
                'id': 'oauth-client-details-add',
                'label': '新增API账号',
                'perms': 'oauth-client-details-add,oms-manufacture-oauth-client-details-add'
              },
              {
                'id': 'org-licence-check',
                'label': '审核货主证照信息',
                'perms': 'org-licence-check,oms-manufacture-org-licence-check'
              },
              {
                'id': 'org-legislation-manager',
                'label': '货主受控法规管理',
                'perms': 'org-legislation-manager,oms-manufacture-org-legislation-manager'
              },
              {
                'id': 'org-licence-edit',
                'label': '编辑货主证照信息',
                'perms': 'org-licence-edit,oms-manufacture-org-licence-edit'
              },
              {
                'id': 'oauth-client-details-watch',
                'label': '查看API账号',
                'perms': 'oauth-client-details-watch,oms-manufacture-oauth-client-details-watch'
              },
              {
                'id': 'biz-quick-update',
                'label': '转为货主',
                'perms': 'biz-quick-update'
              },
              {
                'id': 'org-licence-delete',
                'label': '删除货主证照信息',
                'perms': 'org-licence-delete,oms-manufacture-org-licence-delete'
              },
              {
                'id': 'cusService-manager',
                'label': '货主联系人员管理',
                'perms': 'cusService-manager,oms-manufacture-org-legislation-manager'
              },
              {
                'id': 'org-scope-manager',
                'label': '单位经营范围管理',
                'perms': 'org-scope-manager,oms-manufacture-org-scope-manager'
              },
              {
                'id': 'oauth-client-details-reset',
                'label': '重置API账号',
                'perms': 'oauth-client-details-reset,oms-manufacture-oauth-client-details-reset'
              },
              {
                'id': 'biz-watch',
                'label': '查看单位',
                'perms': 'biz-watch,oms-manufacture-biz-watch'
              },
              {
                'id': 'biz-quick-audit',
                'label': '基础信息审核审核',
                'perms': 'biz-quick-audit'
              },
              {
                'id': 'biz-base-info-check',
                'label': '模块审核单位基础信息',
                'perms': 'biz-base-info-check'
              },
              {
                'id': 'biz-delete',
                'label': '删除单位',
                'perms': 'biz-delete'
              },
              {
                'id': 'biz-edit',
                'label': '编辑单位基础信息',
                'perms': 'biz-edit, oms-manufacture-biz-edit'
              },
              {
                'id': 'org-licence-add',
                'label': '新增货主证照信息',
                'perms': 'org-licence-add,oms-manufacture-org-licence-add'
              }
            ],
            'perms': 'goods-owner-baseInfo'
          },
          {
            'id': 'goods-owner-instant-inventory',
            'label': '即时库存',
            'perms': 'goods-owner-instant-inventory'
          },
          {
            'id': 'oms-business-unit-org-access-watch',
            'label': '角色管理',
            'perms': 'oms-business-unit-org-access-watch,oms-manufacture-oms-access-role-watch'
          },
          {
            'id': 'oms-business-unit-add',
            'label': '新增单位',
            'perms': 'oms-business-unit-add'
          },
          {
            'id': 'goods-owner-dc-order',
            'label': '疾控订单',
            'children': [
              {
                'id': 'goods-owner-dc-order-create',
                'label': '生成销售订单',
                'perms': 'goods-owner-dc-order-create'
              }
            ],
            'perms': 'goods-owner-dc-order'
          },
          {
            'id': 'goods-owner-addressInfo',
            'label': '仓库地址',
            'children': [
              {
                'id': 'org-addressInfo-add',
                'label': '新增货主仓库信息',
                'perms': 'org-addressInfo-add,oms-manufacture-org-addressInfo-add'
              },
              {
                'id': 'org-addressInfo-bizForbid',
                'label': '业务停用货主仓库信息',
                'perms': 'org-addressInfo-bizForbid,oms-manufacture-org-addressInfo-bizForbid'
              },
              {
                'id': 'org-addressInfo-quick-audit',
                'label': '一键审核货主仓库信息',
                'perms': 'org-addressInfo-quick-audit,oms-manufacture-org-addressInfo-audit'
              },
              {
                'id': 'org-addressInfo-audit',
                'label': '审核货主仓库信息',
                'perms': 'org-addressInfo-audit,oms-manufacture-org-addressInfo-audit'
              },
              {
                'id': 'org-addressInfo-query',
                'label': '查询货主仓库信息',
                'perms': 'org-addressInfo-query,oms-manufacture-org-addressInfo-query'
              },
              {
                'id': 'org-addressInfo-start',
                'label': '启用货主仓库信息',
                'perms': 'org-addressInfo-start,oms-manufacture-org-addressInfo-start'
              },
              {
                'id': 'org-addressInfo-update',
                'label': '编辑货主仓库信息',
                'perms': 'org-addressInfo-update,oms-manufacture-org-addressInfo-update'
              },
              {
                'id': 'org-addressInfo-forbid',
                'label': '停用货主仓库信息',
                'perms': 'org-addressInfo-forbid,oms-manufacture-org-addressInfo-forbid'
              }
            ],
            'perms': 'goods-owner-addressInfo'
          },
          {
            'id': 'oms-business-unit-org-user-watch',
            'label': '用户管理',
            'perms': 'oms-business-unit-org-user-watch,oms-manufacture-oms-org-user-watch'
          },
          {
            'id': 'oms-business-unit-query',
            'label': '查看业务单位',
            'perms': 'oms-business-unit-query'
          }
        ],
        'perms': 'oms-business-unit'
      }
    ],
    'perms': 'oms-business-info'
  },
  {
    'id': 'oms-common-baseInfo',
    'label': '业务控制项目库',
    'children': [
      {
        'id': 'oms-exception-item',
        'label': '异常项目',
        'children': [
          {
            'id': 'qualityExceptionType-attachment-download',
            'label': '异常类型附件下载',
            'perms': 'qualityExceptionType-attachment-download'
          },
          {
            'id': 'qualityExceptionType-forbid',
            'label': '停用异常类型',
            'perms': 'qualityExceptionType-forbid'
          },
          {
            'id': 'qualityExceptionType-update',
            'label': '编辑异常类型',
            'perms': 'qualityExceptionType-update'
          },
          {
            'id': 'qualityExceptionType-add',
            'label': '新增异常类型',
            'perms': 'qualityExceptionType-add'
          },
          {
            'id': 'qualityExceptionType-start',
            'label': '启用异常类型',
            'perms': 'qualityExceptionType-start'
          },
          {
            'id': 'qualityExceptionType-query',
            'label': '查询异常类型',
            'perms': 'qualityExceptionType-query'
          }
        ],
        'perms': 'oms-exception-item'
      },
      {
        'id': 'oms-quality-check-template',
        'label': '质量方案模板库',
        'children': [
          {
            'id': 'quality-programmer-start',
            'label': '启用质量检查方案',
            'perms': 'quality-programmer-start,oms-manufacture-quality-programmer-start'
          },
          {
            'id': 'quality-programmer-forbid',
            'label': '停用质量检查方案',
            'perms': 'quality-programmer-forbid,oms-manufacture-quality-programmer-forbid'
          },
          {
            'id': 'quality-programmer-add',
            'label': '新增质量检查方案',
            'perms': 'quality-programmer-add,oms-manufacture-quality-programmer-add'
          },
          {
            'id': 'quality-programmer-query',
            'label': '查询质量检查方案',
            'perms': 'quality-programmer-query,oms-manufacture-quality-programmer-query'
          },
          {
            'id': 'quality-programmer-update',
            'label': '编辑质量检查方案',
            'perms': 'quality-programmer-update,oms-manufacture-quality-programmer-update'
          }
        ],
        'perms': 'oms-quality-check-template'
      },
      {
        'id': 'oms-quality-inspection-items',
        'label': '质量检查项目',
        'children': [
          {
            'id': 'qualityItem-update',
            'label': '编辑质量检查项目',
            'perms': 'qualityItem-update'
          },
          {
            'id': 'qualityItem-start',
            'label': '启用质量检查项目',
            'perms': 'qualityItem-start'
          },
          {
            'id': 'qualityItem-query',
            'label': '查询质量检查项目',
            'perms': 'qualityItem-query'
          },
          {
            'id': 'qualityItem-attachment-download',
            'label': '质量检查项目附件下载',
            'perms': 'qualityItem-attachment-download'
          },
          {
            'id': 'qualityItem-add',
            'label': '新增质量检查项目',
            'perms': 'qualityItem-add'
          },
          {
            'id': 'qualityItem-forbid',
            'label': '停用质量检查项目',
            'perms': 'qualityItem-forbid'
          }
        ],
        'perms': 'oms-quality-inspection-items'
      }
    ],
    'perms': 'oms-common-baseInfo'
  },
  {
    'id': 'system-config',
    'label': '系统设置',
    'children': [
      {
        'id': 'user-role-manager',
        'label': '角色管理',
        'children': [
          {
            'id': 'oms-access-role-stop',
            'label': '停用角色',
            'perms': 'oms-access-role-stop,oms-manufacture-oms-access-role-stop'
          },
          {
            'id': 'oms-access-role-start',
            'label': '启用角色',
            'perms': 'oms-access-role-start,oms-manufacture-oms-access-role-start'
          },
          {
            'id': 'oms-access-role-edit',
            'label': '编辑角色',
            'perms': 'oms-access-role-edit,oms-manufacture-oms-access-role-edit'
          },
          {
            'id': 'oms-access-role-watch',
            'label': '查看角色',
            'perms': 'oms-access-role-watch,oms-manufacture-oms-access-role-watch'
          },
          {
            'id': 'oms-access-platfrom-role-export',
            'label': '平台用户角色分配导出',
            'perms': 'oms-access-platfrom-role-export'
          },
          {
            'id': 'oms-access-role-delete',
            'label': '删除角色',
            'perms': 'oms-access-role-delete,oms-manufacture-oms-access-role-delete'
          },
          {
            'id': 'oms-access-platfrom-permission-export',
            'label': '平台角色权限导出',
            'perms': 'oms-access-platfrom-permission-export'
          },
          {
            'id': 'oms-access-role-add',
            'label': '新增角色',
            'perms': 'oms-access-role-add,oms-manufacture-oms-access-role-add'
          }
        ],
        'perms': 'user-role-manager'
      },
      {
        'id': 'dict-manager',
        'label': '数据字典',
        'children': [
          {
            'id': 'oms-dict-item-update',
            'label': '编辑数据字典项',
            'perms': 'oms-dict-item-update'
          },
          {
            'id': 'oms-dict-group-query',
            'label': '查询数据字典组',
            'perms': 'oms-dict-group-query'
          },
          {
            'id': 'oms-dict-group-softDelete',
            'label': '删除数据字典组',
            'perms': 'oms-dict-group-softDelete'
          },
          {
            'id': 'oms-dict-item-add',
            'label': '新增数据字典项',
            'perms': 'oms-dict-item-add'
          },
          {
            'id': 'oms-dict-group-add',
            'label': '新增数据字典组',
            'perms': 'oms-dict-group-add'
          },
          {
            'id': 'oms-dict-item-softDelete',
            'label': '删除数据字典项',
            'perms': 'oms-dict-item-softDelete'
          },
          {
            'id': 'oms-dict-group-update',
            'label': '编辑数据字典组',
            'perms': 'oms-dict-group-update'
          },
          {
            'id': 'oms-dict-item-query',
            'label': '查询数据字典项',
            'perms': 'oms-dict-item-query'
          }
        ],
        'perms': 'dict-manager'
      },
      {
        'id': 'organization-user-manager',
        'label': '组织用户管理',
        'children': [
          {
            'id': 'oms-org-user-add',
            'label': '新增货主用户',
            'perms': 'oms-org-user-add,oms-manufacture-oms-manufacture-oms-org-user-add'
          },
          {
            'id': 'oms-org-user-edit',
            'label': '编辑货主用户',
            'perms': 'oms-org-user-edit,oms-manufacture-oms-org-user-edit'
          },
          {
            'id': 'oms-org-user-watch',
            'label': '查看货主用户',
            'perms': 'oms-org-user-watch,oms-manufacture-oms-org-user-watch'
          }
        ],
        'perms': 'organization-user-manager'
      },
      {
        'id': 'platform-user-manager',
        'label': '平台用户管理',
        'children': [
          {
            'id': 'platform-user-add',
            'label': '新增平台用户',
            'perms': 'platform-user-add'
          },
          {
            'id': 'platform-user-edit',
            'label': '编辑平台用户',
            'perms': 'platform-user-edit'
          },
          {
            'id': 'platform-user-watch',
            'label': '查看平台用户',
            'perms': 'platform-user-watch'
          }
        ],
        'perms': 'platform-user-manager'
      }
    ],
    'perms': 'system-config'
  },
  {
    'id': 'file-date-manager',
    'label': '证照材料和批号材料',
    'children': [
      {
        'id': 'oms-licence-manager',
        'label': '厂商证照',
        'perms': 'oms-licence-manager'
      },
      {
        'id': 'oms-approvalNumber-manager',
        'label': '货品批准文号管理',
        'perms': 'oms-approvalNumber-manager'
      },
      {
        'id': 'oms-batchNumber-manager',
        'label': '疫苗批号文件管理',
        'children': [
          {
            'id': 'oms-batchNumber-file-confirm',
            'label': '确认批号文件',
            'perms': 'oms-batchNumber-file-confirm'
          },
          {
            'id': 'oms-batchNumber-file-upload',
            'label': '批号文件上传',
            'perms': 'oms-batchNumber-file-upload'
          },
          {
            'id': 'oms-batchNumber-file-update',
            'label': '批号编辑',
            'perms': 'oms-batchNumber-file-update'
          },
          {
            'id': 'oms-batchNumber-file-delete',
            'label': '批号删除',
            'perms': 'oms-batchNumber-file-delete'
          }
        ],
        'perms': 'oms-batchNumber-manager'
      }
    ],
    'perms': 'file-date-manager'
  },
  {
    'id': 'oms-backstage-config',
    'label': 'OMS后台设置',
    'children': [
      {
        'id': 'oms-order-manager',
        'label': '订单管理',
        'children': [
          {
            'id': 'oms-order-watch',
            'label': '订单查看',
            'perms': 'oms-order-watch'
          }
        ],
        'perms': 'oms-order-manager'
      }
    ],
    'perms': 'oms-backstage-config'
  },
  {
    'id': 'oms-manufacture-menu',
    'label': '委托厂商权限菜单',
    'children': [
      {
        'id': 'oms-manufacture-quality-programmer',
        'label': '质检方案',
        'children': [
          {
            'id': 'oms-manufacture-quality-programmer-query',
            'label': '查询质量检查方案',
            'perms': 'oms-manufacture-quality-programmer-query'
          },
          {
            'id': 'oms-manufacture-quality-programmer-add',
            'label': '新增质量检查方案',
            'perms': 'oms-manufacture-quality-programmer-add'
          },
          {
            'id': 'oms-manufacture-quality-programmer-update',
            'label': '编辑质量检查方案',
            'perms': 'oms-manufacture-quality-programmer-update'
          },
          {
            'id': 'oms-manufacture-quality-programmer-start',
            'label': '启用质量检查方案',
            'perms': 'oms-manufacture-quality-programmer-start'
          },
          {
            'id': 'oms-manufacture-quality-programmer-forbid',
            'label': '停用质量检查方案',
            'perms': 'oms-manufacture-quality-programmer-forbid'
          }
        ],
        'perms': 'oms-manufacture-quality-programmer'
      },
      {
        'id': 'oms-manufacture-relation',
        'label': '业务关系',
        'children': [
          {
            'id': 'oms-manufacture-org-relation-check',
            'label': '审核业务关系',
            'perms': 'oms-manufacture-org-relation-check'
          },
          {
            'id': 'oms-manufacture-org-relation-add',
            'label': '新增业务关系',
            'perms': 'oms-manufacture-org-relation-add'
          },
          {
            'id': 'oms-manufacture-org-relation-delete',
            'label': '删除业务关系',
            'perms': 'oms-manufacture-org-relation-delete'
          },
          {
            'id': 'oms-manufacture-org-relation-edit',
            'label': '编辑业务关系',
            'perms': 'oms-manufacture-org-relation-edit'
          }
        ],
        'perms': 'oms-manufacture-relation'
      },
      {
        'id': 'oms-manufacture-order-manager',
        'label': '订单管理',
        'children': [
          {
            'id': 'oms-manufacture-dc-order',
            'label': '疾控订单',
            'children': [
              {
                'id': 'oms-manufacture-dc-order-create',
                'label': '生成销售订单',
                'perms': 'oms-manufacture-dc-order-create'
              }
            ],
            'perms': 'oms-manufacture-dc-order'
          },
          {
            'id': 'oms-manufacture-out-order',
            'label': '出库订单',
            'children': [
              {
                'id': 'oms-manufacture-out-order-status-confirm',
                'label': '确认出库订单',
                'perms': 'oms-manufacture-out-order-status-confirm'
              },
              {
                'id': 'oms-manufacture-out-order-query',
                'label': '查看出库单',
                'perms': 'oms-manufacture-out-order-query'
              },
              {
                'id': 'oms-manufacture-out-order-add',
                'label': '新增出库订单',
                'perms': 'oms-manufacture-out-order-add'
              },
              {
                'id': 'oms-manufacture-out-order-status-cancel',
                'label': '取消出库订单',
                'perms': 'oms-manufacture-out-order-status-cancel'
              }
            ],
            'perms': 'oms-manufacture-out-order'
          },
          {
            'id': 'oms-manufacture-in-order',
            'label': '移入库订单',
            'children': [
              {
                'id': 'oms-manufacture-in-order-status-confirm',
                'label': '确认入库订单',
                'perms': 'oms-manufacture-in-order-status-confirm'
              },
              {
                'id': 'oms-manufacture-in-order-query',
                'label': '查看入库单',
                'perms': 'oms-manufacture-in-order-query'
              },
              {
                'id': 'oms-manufacture-in-order-add',
                'label': '新增入库订单',
                'perms': 'oms-manufacture-in-order-add'
              },
              {
                'id': 'oms-manufacture-in-order-status-cancel',
                'label': '取消入库订单',
                'perms': 'oms-manufacture-in-order-status-cancel'
              }
            ],
            'perms': 'oms-manufacture-in-order'
          }
        ],
        'perms': 'oms-manufacture-order-manager'
      },
      {
        'id': 'oms-manufacture-system-config',
        'label': '系统设置',
        'children': [
          {
            'id': 'oms-manufacture-bankInfo',
            'label': '财务信息',
            'children': [
              {
                'id': 'oms-manufacture-org-bankInfo-add',
                'label': '新增银行信息',
                'perms': 'oms-manufacture-org-bankInfo-add'
              },
              {
                'id': 'oms-manufacture-org-finance-update',
                'label': '编辑单位财务信息',
                'perms': 'oms-manufacture-org-finance-update'
              },
              {
                'id': 'oms-manufacture-org-bankInfo-query',
                'label': '查询银行信息',
                'perms': 'oms-manufacture-org-bankInfo-query'
              },
              {
                'id': 'oms-manufacture-org-finance-watch',
                'label': '查看单位财务信息',
                'perms': 'oms-manufacture-org-finance-watch'
              },
              {
                'id': 'oms-manufacture-org-bankInfo-audit',
                'label': '审核银行信息',
                'perms': 'oms-manufacture-org-bankInfo-audit'
              },
              {
                'id': 'oms-manufacture-org-finance-audit',
                'label': '审核单位财务信息',
                'perms': 'oms-manufacture-org-finance-audit'
              },
              {
                'id': 'oms-manufacture-org-bankInfo-forbid',
                'label': '停用银行信息',
                'perms': 'oms-manufacture-org-bankInfo-forbid'
              },
              {
                'id': 'oms-manufacture-org-bankInfo-start',
                'label': '启用银行信息',
                'perms': 'oms-manufacture-org-bankInfo-start'
              },
              {
                'id': 'oms-manufacture-org-finance-add',
                'label': '新增单位财务信息',
                'perms': 'oms-manufacture-org-finance-add'
              },
              {
                'id': 'oms-manufacture-org-bankInfo-update',
                'label': '编辑银行信息',
                'perms': 'oms-manufacture-org-bankInfo-update'
              }
            ],
            'perms': 'oms-manufacture-bankInfo'
          },
          {
            'id': 'oms-manufacture-user-role-manager',
            'label': '角色管理',
            'children': [
              {
                'id': 'oms-manufacture-oms-access-role-start',
                'label': '启用角色',
                'perms': 'oms-manufacture-oms-access-role-start'
              },
              {
                'id': 'oms-manufacture-oms-access-role-watch',
                'label': '查看角色',
                'perms': 'oms-manufacture-oms-access-role-watch'
              },
              {
                'id': 'oms-manufacture-oms-access-role-delete',
                'label': '删除角色',
                'perms': 'oms-manufacture-oms-access-role-delete'
              },
              {
                'id': 'oms-manufacture-oms-access-role-edit',
                'label': '编辑角色',
                'perms': 'oms-manufacture-oms-access-role-edit'
              },
              {
                'id': 'oms-manufacture-oms-access-role-stop',
                'label': '停用角色',
                'perms': 'oms-manufacture-oms-access-role-stop'
              },
              {
                'id': 'oms-manufacture-oms-access-role-add',
                'label': '新增角色',
                'perms': 'oms-manufacture-oms-access-role-add'
              }
            ],
            'perms': 'oms-manufacture-user-role-manager'
          },
          {
            'id': 'oms-manufacture-baseInfo',
            'label': '基础信息',
            'children': [
              {
                'id': 'oms-manufacture-org-licence-add',
                'label': '新增货主证照信息',
                'perms': 'oms-manufacture-org-licence-add'
              },
              {
                'id': 'oms-manufacture-oauth-client-details-add',
                'label': '新增API账号',
                'perms': 'oms-manufacture-oauth-client-details-add'
              },
              {
                'id': 'oms-manufacture-oauth-client-details-reset',
                'label': '重置API账号',
                'perms': 'oms-manufacture-oauth-client-details-reset'
              },
              {
                'id': 'oms-manufacture-oauth-client-details-watch',
                'label': '查看API账号',
                'perms': 'oms-manufacture-oauth-client-details-watch'
              },
              {
                'id': 'oms-manufacture-org-scope-manager',
                'label': '单位经营范围管理',
                'perms': 'oms-manufacture-org-scope-manager'
              },
              {
                'id': 'oms-manufacture-org-licence-delete',
                'label': '删除货主证照信息',
                'perms': 'oms-manufacture-org-licence-delete'
              },
              {
                'id': 'oms-manufacture-org-legislation-manager',
                'label': '货主受控法规管理',
                'perms': 'oms-manufacture-org-legislation-manager'
              },
              {
                'id': 'oms-manufacture-org-licence-check',
                'label': '审核货主证照信息',
                'perms': 'oms-manufacture-org-licence-check'
              },
              {
                'id': 'oms-manufacture-biz-watch',
                'label': '查看单位',
                'perms': 'oms-manufacture-biz-watch'
              },
              {
                'id': 'oms-manufacture-biz-edit',
                'label': '编辑单位基础信息',
                'perms': 'oms-manufacture-biz-edit'
              },
              {
                'id': 'oms-manufacture-org-licence-edit',
                'label': '编辑货主证照信息',
                'perms': 'oms-manufacture-org-licence-edit'
              },
              {
                'id': 'oms-manufacture-cusService-manager',
                'label': '货主联系人员管理',
                'perms': 'oms-manufacture-cusService-manager'
              }
            ],
            'perms': 'oms-manufacture-baseInfo'
          },
          {
            'id': 'oms-manufacture-addressInfo',
            'label': '仓库地址',
            'children': [
              {
                'id': 'oms-manufacture-org-addressInfo-bizForbid',
                'label': '业务停用货主仓库信息',
                'perms': 'oms-manufacture-org-addressInfo-bizForbid'
              },
              {
                'id': 'oms-manufacture-org-addressInfo-update',
                'label': '编辑货主仓库信息',
                'perms': 'oms-manufacture-org-addressInfo-update'
              },
              {
                'id': 'oms-manufacture-org-addressInfo-start',
                'label': '启用货主仓库信息',
                'perms': 'oms-manufacture-org-addressInfo-start'
              },
              {
                'id': 'oms-manufacture-org-addressInfo-add',
                'label': '新增货主仓库信息',
                'perms': 'oms-manufacture-org-addressInfo-add'
              },
              {
                'id': 'oms-manufacture-org-addressInfo-audit',
                'label': '审核货主仓库信息',
                'perms': 'oms-manufacture-org-addressInfo-audit'
              },
              {
                'id': 'oms-manufacture-org-addressInfo-forbid',
                'label': '停用货主仓库信息',
                'perms': 'oms-manufacture-org-addressInfo-forbid'
              },
              {
                'id': 'oms-manufacture-org-addressInfo-query',
                'label': '查询货主仓库信息',
                'perms': 'oms-manufacture-org-addressInfo-query'
              }
            ],
            'perms': 'oms-manufacture-addressInfo'
          },
          {
            'id': 'oms-manufacture-organization-user-manager',
            'label': '用户管理',
            'children': [
              {
                'id': 'oms-manufacture-oms-org-user-edit',
                'label': '编辑用户',
                'perms': 'oms-manufacture-oms-org-user-edit'
              },
              {
                'id': 'oms-manufacture-oms-manufacture-oms-org-user-add',
                'label': '新增用户',
                'perms': 'oms-manufacture-oms-manufacture-oms-org-user-add'
              },
              {
                'id': 'oms-manufacture-oms-org-user-watch',
                'label': '查看用户',
                'perms': 'oms-manufacture-oms-org-user-watch'
              }
            ],
            'perms': 'oms-manufacture-organization-user-manager'
          }
        ],
        'perms': 'oms-manufacture-system-config'
      },
      {
        'id': 'oms-manufacture-instant-inventory',
        'label': '即时库存',
        'perms': 'oms-manufacture-instant-inventory'
      },
      {
        'id': 'oms-manufacture-stock-detail',
        'label': '出入库明细',
        'perms': 'oms-manufacture-stock-detail'
      },
      {
        'id': 'oms-manufacture-goodsInfo',
        'label': '货品资料',
        'children': [
          {
            'id': 'oms-manufacture-org-goods-edit',
            'label': '编辑货主货品信息',
            'perms': 'oms-manufacture-org-goods-edit'
          },
          {
            'id': 'oms-manufacture-org-goods-add',
            'label': '新增货主货品信息',
            'perms': 'oms-manufacture-org-goods-add'
          },
          {
            'id': 'oms-manufacture-org-goods-delete',
            'label': '删除货主货品信息',
            'perms': 'oms-manufacture-org-goods-delete'
          },
          {
            'id': 'oms-manufacture-org-goods-check',
            'label': '审核货主货品信息',
            'perms': 'oms-manufacture-org-goods-check'
          }
        ],
        'perms': 'oms-manufacture-goodsInfo'
      }
    ],
    'perms': 'oms-manufacture-menu'
  }
];
