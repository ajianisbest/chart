import {MessageBox} from 'element-ui';
import {measure, dimension} from '@/resources';
import Vue from 'vue';

export default {
  rename: function (modal, id, title = '') {
    return new Promise(function (resolve, reject) {
      if (!modal || !id) {
        reject();
      } else {
        MessageBox.prompt('新的名称', '重命名', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          inputPlaceholder: '新的名称',
          inputValue: title
        }).then(({value}) => {
          modal.update(id, {title: value}).then(res => {
            resolve(res);
          });
        });
      }
    });
  },
  delete: function (modal, id) {
    return new Promise(function (resolve, reject) {
      if (!modal || !id) {
        reject();
      } else {
        MessageBox.confirm('此操作将隐藏该信息, 是否继续?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          modal.delete(id).then(res => {
            resolve(res);
          });
        });
      }
    });
  },
  dTom: function (feild, vm) {
    const h = vm.$createElement;
    return new Promise(function (resolve, reject) {
      if (!feild || !feild.id) {
        reject();
      } else {
        MessageBox({
          title: '提示',
          message: h('p', null, '确认转换成维度信息?'),
          showCancelButton: true,
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          measure.delete(feild.id).then(res => {
            dimension.save({
              title: feild.title,
              fieldId: feild.id
            }).then(res => {
              resolve(res);
            });
          });

        });
      }
    });
  },
  mTod: function (feild, vm) {
    const h = vm.$createElement;
    const functionList = vm.aggregateFunList;
    return new Promise(function (resolve, reject) {
      if (!feild || !feild.id) {
        reject();
      } else {
        let optionList = [];
        functionList.forEach(item => {
          optionList.push(h('el-option', {props: {label: item.title, value: item.name}}, null));
        });
        vm.functionName = '';
        MessageBox({
          title: '转换成度量信息?',
          message: h('el-select', {
            ref: 'functionName', props: {value: vm.functionName}, on: {
              change: (e => {
                vm.functionName = e;
              })
            }
          }, optionList),
          showCancelButton: true,
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          /* modal.delete(id).then(res => {
           resolve(res);
           });*/
        });
      }
    });
  }
};
