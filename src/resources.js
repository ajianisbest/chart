import {Notification} from 'element-ui';
import axios from 'axios';
import Vue from 'vue';

const http = axios.create({
  baseURL: process.env.NODE_API,
  timeout: 30000,
  withCredentials: false
});

http.interceptors.response.use(response => {
  if (response.data && response.data.code === 0) {
    return response;
  } else {
    let msg = 'error';
    if (response.data) {
      msg = response.data.msg;
    }
    Notification.warning({
      message: msg
    });
  }

}, error => {
  let noticeTipKey = 'noticeError';
  let notice = window.localStorage.getItem(noticeTipKey);
  let response = error.response;

  if (notice === '1' && response.status !== 401) {
    return Promise.reject(error);
  } else {
    window.localStorage.setItem(noticeTipKey, '1');
  }
  if (!response || response.status === 500) {
    Notification.warning({
      message: '服务器太久没有响应, 请重试',
      onClose: function () {
        window.localStorage.removeItem(noticeTipKey);
      }
    });
  }
  if (response.status === 401) { //  Unauthorized, redirect to login
    let lastUrl = window.localStorage.getItem('lastUrl');
    if (!lastUrl || lastUrl.indexOf('/base/dict') === -1) {
    }
    window.location.href = '#/login';
    return Promise.reject(error);
  }
  if (response.status === 403) {
    Notification.error({
      message: '非法请求',
      onClose: function () {
        window.localStorage.removeItem(noticeTipKey);
      }
    });
  }

  if (response.status === 502) {
    Notification.error({
      message: '网络异常',
      onClose: function () {
        window.localStorage.removeItem(noticeTipKey);
      }
    });
  }
  return Promise.reject(error);
});

Vue.prototype.$http = http;

const dtoList = [
  'chart',
  'project',
  'database',
  'datasource',
  'dataset',
  'joindataset',
  'field',
  'dimension',
  'measure'
];
let exports = {http: http};
dtoList.forEach(dto => {
  exports[dto] = resource('/' + dto, http, {});
});
module.exports = exports;
module.exports.default = exports;

/*
 export const chart = resource('/chart', http, {});
 export const project = resource('/project', http, {});
 export const database = resource('/database', http, {});
 export const datasource = resource('/datasource', http, {});
 export const dataset = resource('/dataset', http, {});
 export const joindataset = resource('/joindataset', http, {});*/

/**
 * create vue-resource's resource like object
 *
 * Default Actions
 *   get: {method: 'GET'}
 *   save: {method: 'POST'}
 *   query: {method: 'GET'}¬
 *   update: {method: 'PUT'}
 *   delete: {method: 'DELETE'}
 *
 * @param path the resource path
 * @param http axios instance
 * @param actions custom actions
 * @returns the resource object
 */
function resource(path, http, actions) {
  let obj = {
    get: id => http.get(path + '/' + id),
    save: obj => http.post(path, obj),
    query: params => http.get(path, {params}),
    update: (id, obj) => {
      let url = path + '/' + id;
      if (typeof (id) === 'object') {
        url = path;
        obj = id;
      }
      return http.put(url, obj);
    },
    delete: id => http.delete(path + '/' + id)
  };
  return Object.assign(obj, actions);
}

