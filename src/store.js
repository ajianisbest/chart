import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

//  需要维护的状态
const state = {
  uploadUrl: 'https:// jsonplaceholder.typicode.com/posts/',
  user: {},
  dict: {},
  permissions: [],
  permList: {},
  roleList: {},
  allRoleMap: {},
  attachmentDialog: {attachmentId: 0, open: false},
  currentDatasetId: {},
  bodyHeight: 0,
  bodyWidth: 0,
  chartOptChange: false,
  aggregateFunList: []
};
let bodyLeft = window.localStorage.getItem('bodyLeft');
if (bodyLeft) {
  state.bodySize.left = bodyLeft;
}
const mutations = {
  initUser(state, data) {
    try {
      if (data && data.userId) {
        window.localStorage.setItem('user', JSON.stringify(data));
        state.user = data;
      }
    } catch (e) {

    }
  },
  initAllRoleMap(state, data) {
    state.allRoleMap = data;
  },
  initDict(state, data) {
    state.dict = data;
  },
  initPermissions(state, data) {
    state.permissions = data;
  },
  initPermList(state, data) {
    state.permList = data;
  },
  changeAttachment(state, data) {
    if (state.attachmentDialog.attachmentId === data) {
      state.attachmentDialog.open = true;
    } else {
      state.attachmentDialog.attachmentId = data;
    }
  },
  openAttachmentDialog(state) {
    state.attachmentDialog.open = true;
  },
  closeAttachmentDialog(state) {
    state.attachmentDialog.open = false;
  },

  setBodyWidth(state, data) {
    state.bodyWidth = data;
  },

  setBodyHeight(state, data) {
    state.bodyHeight = data;
  },
  chartOptionChange(state, data) {
    state.chartOptChange = data;
  },
  changeCurrentDatasetId(state, data) {
    state.currentDatasetId = data;
  },
  initAggregateFunList(state, data) {
    state.aggregateFunList = data;
  }
};

export default new Vuex.Store({
  state,
  mutations
});
