export default {
  data() {
    return {
      timer: '',
      chartList: []
    };
  },
  watch: {
    form: {
      handler(form) {
        this.init();
      },
      deep: true
    }
  },
  mounted() {
    this.init();
  },
  methods: {
    axisFun(name) {
      let axisData = [];
      this.form.originalData.forEach(item => {
        axisData.indexOf(item[name]) === -1 ? axisData.push(item[name]) : '';
      });
      return axisData;
    }
  }
};
