/**
 * Show migrating guide in browser console.
 *
 * Usage:
 * import Migrating from 'element-ui/src/mixins/migrating';
 *
 * mixins: [Migrating]
 *
 * add getMigratingConfig method for your component.
 *  getMigratingConfig() {
 *    return {
 *      props: {
 *        'allow-no-selection': 'allow-no-selection is removed.',
 *        'selection-mode': 'selection-mode is removed.'
 *      },
 *      events: {
 *        selectionchange: 'selectionchange is renamed to selection-change.'
 *      }
 *    };
 *  },
 */
import {http, chart} from '@/resources';

export default {
  mounted() {

  },
  methods: {
    addChart() {
      let projectId = this.$route.params.projectId;
      this.$prompt('请输入图表标题', '新增图表', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        inputPattern: /.+/,
        inputErrorMessage: '请输入标题'
      }).then(({value}) => {
        chart.save({title: value, projectId: projectId}).then(res => {
          if (res.data && res.data.chartId) {
            this.$router.push({path: `/project/${projectId}/chart/${res.data.chartId}/ds`});
          }
        });
      });
    },
    addChartDashboard() {
      this.$notify.info({
        message: '准备中'
      });
    },
    addStory() {
      this.$notify.info({
        message: '准备中'
      });
    }
  }
};
