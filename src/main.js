import Vue from 'vue';
import router from './routers';
import tinyVue from '@/lib/tinyVue';
import moment from 'moment';
import 'moment/locale/zh-cn';
import './assets/element-variables.scss';

import './assets/iconfont/iconfont.css';

import Vuex from 'vuex';
import store from './store';

Vue.use(require('vue-moment'), {moment});
Vue.use(tinyVue);
Vue.use(Vuex);
Vue.filter('date', function (dateTime) {
  if (!dateTime) return '';
  return moment(dateTime).format('YYYY-MM-DD');
});
Vue.filter('time', function (dateTime) {
  if (!dateTime) return '';
  return moment(dateTime).format('YYYY-MM-DD HH:mm:ss');
});

new Vue({
  template: '<router-view id="app"></router-view>',
  router,
  store
}).$mount('#app');
