const route = [
  {
    path: '/',
    component: resolve => require(['./pages/index.vue'], resolve),
    meta: {moduleId: 'product', title: '项目'},
    children: [
      {
        path: '/',
        component: resolve => require(['./pages/project/index.vue'], resolve),
        meta: {moduleId: 'product', title: '项目'}
      },
      {
        path: '/chart/:chartId',
        component: resolve => require(['./pages/chart/charts/charts.vue'], resolve),
        meta: {moduleId: 'chart', title: '图表'}
      },
      {
        path: '/project/:projectId/charts',
        name: 'charts',
        component: resolve => require(['./pages/project/chars.vue'], resolve),
        meta: {moduleId: 'charts', title: '图表列表', icon: 'chart', perm: ''}
      },
      {
        path: '/project/:projectId/dataSource',
        name: 'dataSource',
        component: resolve => require(['./pages/dataSource/list.vue'], resolve),
        meta: {moduleId: 'dataSource', title: '数据源列表', icon: 'data', perm: ''}
      },
      {
        path: '/project/:projectId/chart/:chartId',
        component: resolve => require(['./pages/chart/show.vue'], resolve),
        meta: {moduleId: 'chart', title: '图表详情', perm: ''},
        children: [
          {
            path: '/project/:projectId/chart/:chartId',
            component: resolve => require(['./pages/chart/charts/charts.vue'], resolve),
            meta: {moduleId: 'chart', title: '图表详情', perm: ''}
          }
        ]
      },
      {
        path: '/project/:projectId/chart/:chartId/ds',
        component: resolve => require(['./pages/dataSet/show.vue'], resolve),
        meta: {moduleId: 'chartDs', title: '图表详情', perm: ''},
        children: [
          {
            path: '/project/:projectId/chart/:chartId/ds',
            component: resolve => require(['./pages/dataSet/ds.vue'], resolve),
            meta: {moduleId: 'chart', title: '数据源', perm: ''}
          }
        ]
      }
    ]

  }
];
export default route;
